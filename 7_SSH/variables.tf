variable "location" {
  type = string
  default = "eastus2"
}
variable "usuario" {
  type = string
  default = "usuario"
}
variable "password" {
  type = string
  default = "P4$$w0rd12345"
}
variable "ssh" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD1o0mfQ0EQp5RF+Lbq5PwLhWytS/8LhAzhTadlgqsA2wyhGhhPapISUzJdWDVPsqXDaEfi5lCr303lU929TPHvIeIx4oolNCu4vO4W3Ijz50k/o2eMZUkWYNtCHNtSTH1ZHlo+OW3Uq2h2g+Tr6+P77/4flpYqtR99OGG5Hfyf1J7VwuUhOhNMU56Og0VXDoI7YQQFTZ02uTTYDHyqpUuuD/m3S+kELncttiHEtaqvWqYc4Bn7f+Lo0oW9ZAGg/yarvQeNJAywCea87a3Zjwc1U79PcQsVXdcD/caE1taPsJMh5zfiSrSrTXov3UFyvj9kXAni0MDdhflj+EYUmYMx frs@frs-pc"
}
